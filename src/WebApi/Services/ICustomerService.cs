﻿using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Services
{

    public interface ICustomerService
    {
        public Task<Customer> GetAsync(long id);
        public Task<long> CreateCustomerAsync(Customer customer);
    }

}
