﻿using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly AppDbContext _db;

        public CustomerService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<long> CreateCustomerAsync(Customer customer)
        {
            var customerExist = GetAsync(customer.Id).Result!=null;
            if (customerExist)
                return -1; 
            await _db.Customer.AddAsync(customer);
            await _db.SaveChangesAsync();
            return customer.Id;
        }

        public async Task<Customer> GetAsync(long id)
        {
            var result = await _db.Customer.FindAsync(id);

            return result;
        }
    }
}
