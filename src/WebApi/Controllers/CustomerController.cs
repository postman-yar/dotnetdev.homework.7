using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _service;
        public CustomerController(ICustomerService service)
        {
            _service = service;
        }

        [HttpGet("{id:long}")]   
        public ActionResult GetCustomerAsync([FromRoute] long id)
        {
            var result = _service.GetAsync(id);
            if (result.Result == null)
                return NotFound(new { message = $"User with id '{id}' not found." });
            return Ok(result.Result);
        }

        [HttpPost("")]   
        public ActionResult CreateCustomerAsync([FromBody] Customer customer)
        {
            var result = _service.CreateCustomerAsync(customer);
            if (result.Result==-1)
                return Conflict(new { message = $"An existing record with the id '{customer.Id}' was already found." });
            return Ok(result.Result);
        }
    }
}