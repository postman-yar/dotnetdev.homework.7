﻿using System;

namespace WebClient
{
    static class Program
    {
        static void Main(string[] args)
        {
            HTTPHelper.HTTPHelper helper = new HTTPHelper.HTTPHelper();
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Выберите действие:");
                Console.WriteLine(">1. Получить клиента");
                Console.WriteLine(">2. Добавить клиента");
                Console.WriteLine(">0. Выход");

                var action = Console.ReadLine();
                switch (action)
                {
                    case "0":
                        return;
                    case "1":
                        GetCustomer(helper);
                        break;
                    case "2":
                        WriteCustomer(helper);
                        break;
                }
            }
        }

        static void GetCustomer(HTTPHelper.HTTPHelper helper)
        {
            Console.WriteLine("Введите id:");
            var id = Console.ReadLine();
            PrintCustomerFromServer(helper, int.Parse(id));
        }

        static void PrintCustomerFromServer(HTTPHelper.HTTPHelper helper, int id)
        {
            var res = helper.GetCustomerById(id.ToString());
            if (res == null)
                Console.WriteLine($"Покупатель с id: {id} не найден");
            else
                Console.WriteLine($"Покупатель: {res}");
        }

        static void WriteCustomer(HTTPHelper.HTTPHelper helper)
        {
            var id = helper.PostCustomer(RandomCustomer());
            if (id > 0)
                PrintCustomerFromServer(helper, id);
            else
                Console.WriteLine("Не удалось записать покупателя");
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            var rnd = new Random();
            return new CustomerCreateRequest("Firstname_" + rnd.Next(), "LastName" + rnd.Next());
        }
    }
}