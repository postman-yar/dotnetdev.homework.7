﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using WebClient;

namespace HTTPHelper
{
    public class HTTPHelper
    {
        public HttpClient _client;

        public HTTPHelper()
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:5001/")
            };
            _client = client;
        }

        public Customer GetCustomerById(string id)
        {
            var url = "customers/" + id;
            var response = _client.GetAsync(url);

            var result = response.Result;
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var responseBodyString = response.Result.Content.ReadAsStringAsync().Result;
                JsonSerializerOptions options = new JsonSerializerOptions(JsonSerializerDefaults.Web)
                {
                    WriteIndented = true
                };
                var customer = JsonSerializer.Deserialize<Customer>(responseBodyString, options);
                return customer;
            }

            return null;
        }

        public int PostCustomer(CustomerCreateRequest customer)
        {
            var stringPayload = JsonSerializer.Serialize(customer);

            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            var response = _client.PostAsync("customers", httpContent);
            var result = response.Result;
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var customerId = int.Parse(response.Result.Content.ReadAsStringAsync().Result);
                return customerId;
            }
            return -1;
        }

    }
}
